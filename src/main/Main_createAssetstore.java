package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.dspace.core.Utils;
import org.postgresql.Driver;

import oracle.jdbc.driver.OracleDriver;

public class Main_createAssetstore {

	public static void main(String[] args) {
//		 args =new String[2];
//		 args[0]="/home/aroman/workspace/es_ucm_backend/config";
//		 args[1]="/home/aroman/workspace/es_ucm_backend/assetstore";
//		 args[0]="D:/programacion/java/workspace/dspace_i_buah_v6/config";
//		 args[1]="D:/programacion/java/workspace/dspace_i_buah_v6/assetstore";
		if (args == null || args.length < 1) {
			System.out.println("Usage: java -jar createAssetstore [dspaceiConfigDir] {[assetstore]}");
			System.exit(0);
		}
		try {
			String cfgDir = args[0];
			String assetstoreparam=null;
			if(args.length>1){
				assetstoreparam=args[1];
			} 
			try {
				File fileCfgDir = new File(cfgDir);
				if (fileCfgDir.exists()) {
					Properties propertiesDspaceCfg = new Properties();
					propertiesDspaceCfg.load(new FileReader(cfgDir + "/dspace.cfg"));
					Properties propertiesLocalCfg = new Properties();
					File fLocal=new File(cfgDir + "/local.cfg");
					if(fLocal.exists()){
						propertiesLocalCfg.load(new FileReader(cfgDir + "/local.cfg"));
					}

					String dbUrl = propertiesDspaceCfg.getProperty("db.url");
					String dbUsername = propertiesDspaceCfg.getProperty("db.username");
					String dbPassword = propertiesDspaceCfg.getProperty("db.password");
					String assetstoreDir = propertiesDspaceCfg.getProperty("assetstore.dir");

					if (propertiesLocalCfg.getProperty("db.url") != null) {
						dbUrl = propertiesLocalCfg.getProperty("db.url");
					}
					if (propertiesLocalCfg.getProperty("db.username") != null) {
						dbUsername = propertiesLocalCfg.getProperty("db.username");
					}
					if (propertiesLocalCfg.getProperty("db.password") != null) {
						dbPassword = propertiesLocalCfg.getProperty("db.password");
					}
					if (propertiesLocalCfg.getProperty("assetstore.dir") != null) {
						assetstoreDir = propertiesLocalCfg.getProperty("assetstore.dir");
					}

					// Si lo recibimos por parametro, lo pisamos.
					if(StringUtils.isNotBlank(assetstoreparam)){
						assetstoreDir=assetstoreparam;
					}

					// por si el assetstore tiene ${algo}
					assetstoreDir = variableAssetstore(assetstoreDir, propertiesDspaceCfg, propertiesLocalCfg);

					DriverManager.registerDriver(new Driver());
					DriverManager.registerDriver(new OracleDriver());

					Connection con = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
					StringBuilder query = new StringBuilder();
					query.append("SELECT a.internal_id, b.mimetype " + "FROM bitstream a, bitstreamformatregistry b "
							+ "WHERE a.bitstream_format_id = b.bitstream_format_id");
					Statement st = con.createStatement();
					ResultSet rs = st.executeQuery(query.toString());

					createDirs(getResults(rs), assetstoreDir,con);
					rs.close();
					st.close();
					con.close();
				}

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}	
	}		

	// Muy mejorable. Solo admite una variable, y faltan comprobaciones
	private static String variableAssetstore(String assetstoreDir, Properties propertiesDspaceCfg,
			Properties propertiesLocalCfg) {
		if (assetstoreDir.indexOf("${") != -1 && assetstoreDir.indexOf("}") != -1) {
			String var = assetstoreDir.substring(assetstoreDir.indexOf("${") + 2, assetstoreDir.indexOf("}"));
			String varValue = propertiesLocalCfg.getProperty(var);
			if (varValue == null) {
				varValue = propertiesDspaceCfg.getProperty(var);
			}
			if (varValue != null) {
				return assetstoreDir.replace("${" + var + "}", varValue);

			} else {
				System.out.println("Parametrizado pero falta en la configuracion la variable:" + var);
				return assetstoreDir;
			}
		} else {
			return assetstoreDir;
		}
	}

	private static void createDirs(List<String> results, String assetstoreDir, Connection con) throws FileNotFoundException, SQLException, IOException {
		int numcreated = 0;
		int numskiped = 0;
		int numnotcreated = 0;
		File file;
		String dir1;
		String dir2;
		String dir3;
		for (String result : results) {
			String[] strings = result.split("\t");
			String fileString = strings[0];
			String extension=null;
			if(strings.length>1){
				 extension= strings[1];
			}
			dir1 = fileString.substring(0, 2);
			dir2 = fileString.substring(2, 4);
			dir3 = fileString.substring(4, 6);

			file = new File(assetstoreDir + "/" + dir1 + "/" + dir2 + "/" + dir3);
			file.mkdirs();
			// Comprobamos que no existe
			File ficheroAEscribir = new File(file.getAbsolutePath() + "/" + fileString);
			if (ficheroAEscribir.exists()) {
				System.out.println("SKIPED: " + ficheroAEscribir);
				numskiped++;
			} else {
				if(createFile(extension, ficheroAEscribir)) {
					numcreated++;
					actualizarChecksum(con,fileString,ficheroAEscribir);
				}else {
					numnotcreated++;
				}
			}
		}
		System.out.println("Numero de ficheros creados:" + numcreated);
		System.out.println("Numero de ficheros saltados:" + numskiped);
		System.out.println("Numero de ficheros no creados" + numnotcreated);

	}

	private static void actualizarChecksum(Connection con,String fileString,File ficheroAEscribir) throws SQLException, FileNotFoundException, IOException {
		StringBuilder query = new StringBuilder();
		query.append("update bitstream set checksum='"+Utils.toHex(Utils.generateChecksumFrom(ficheroAEscribir))+"', size_bytes="+ficheroAEscribir.length()+" where internal_id='"+fileString+"'");
		Statement st = con.createStatement();
		int numChanges = st.executeUpdate(query.toString());
		if(numChanges!=1) {
			System.out.println("No se ha podido actualizar el checksum:"+fileString+":"+ficheroAEscribir);
		}
		
	}

	private static boolean createFile(String extension, File ficheroAEscribir) throws IOException {
		boolean created = false;
		if(extension==null){
			System.out.println("NOT CREATED: " + ficheroAEscribir + "\nEXTENSION: " + extension);
			return created;
		}
		if (extension.contains("plain")) {
			created = createExtensionFile("file.txt", ficheroAEscribir);
		} else if (extension.contains("html")) {
			created = createExtensionFile("file.html", ficheroAEscribir);
		} else if (extension.contains("pdf")) {
			created = createExtensionFile("file.pdf", ficheroAEscribir);
		} else if (extension.contains("xml")) {
			created = createExtensionFile("file.xml", ficheroAEscribir);
		} else if (extension.contains("css")) {
			created = createExtensionFile("file.css", ficheroAEscribir);
		} else if (extension.contains("msword")) {
			created = createExtensionFile("file.doc", ficheroAEscribir);
		} else if (extension.contains("vnd.openxmlformats-officedocument.wordprocessingml.document")) {
			created = createExtensionFile("file.docx", ficheroAEscribir);
		} else if (extension.contains("vnd.ms-powerpoint")) {
			created = createExtensionFile("file.ppt", ficheroAEscribir);
		} else if (extension.contains("vnd.openxmlformats-officedocument.presentationml.presentation")) {
			created = createExtensionFile("file.pptx", ficheroAEscribir);
		} else if (extension.contains("vnd.ms-excel")) {
			created = createExtensionFile("file.xls", ficheroAEscribir);
		} else if (extension.contains("vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
			created = createExtensionFile("file.xlsx", ficheroAEscribir);
		} else if (extension.contains("jpeg")) {
			created = createExtensionFile("file.jpeg", ficheroAEscribir);
		} else if (extension.contains("gif")) {
			created = createExtensionFile("file.gif", ficheroAEscribir);
		} else if (extension.contains("png")) {
			created = createExtensionFile("file.png", ficheroAEscribir);
		} else if (extension.contains("tiff")) {
			created = createExtensionFile("file.tiff", ficheroAEscribir);
		} else if (extension.contains("richtext")) {
			created = createExtensionFile("file.rtf", ficheroAEscribir);
		} else if (extension.contains("vnd.oasis.opendocument.text")) {
			created = createExtensionFile("file.odt", ficheroAEscribir);
		} else {
			System.out.println("NOT CREATED: " + ficheroAEscribir + "\nEXTENSION: " + extension);
		}
		
		return created;
	}
	
	public static boolean createExtensionFile(String extensionName, File targetFile) throws IOException {
		InputStream initialStream = Main_createAssetstore.class.getResourceAsStream("/extensions/"+extensionName);
		
		OutputStream outStream = new FileOutputStream(targetFile);
		byte[] buffer = new byte[8 * 1024];
		int bytesRead;
		while ((bytesRead = initialStream.read(buffer)) != -1) {
			outStream.write(buffer, 0, bytesRead);
		}
		IOUtils.closeQuietly(initialStream);
		IOUtils.closeQuietly(outStream);
		return true;
	}
	
	private static boolean createExtensionFile(File fileCopy, File filePaste) {
		try {
			Files.copy(fileCopy.toPath(), filePaste.toPath(), StandardCopyOption.REPLACE_EXISTING);
			System.out.println("CREATED: " + filePaste);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/*
	private static int createGIFFile(int numcreated, File ficheroAEscribir) {
		try {
			BufferedImage firstImage = ImageIO.read(new File("a.jpeg"));

			
			ImageOutputStream output = 
				      new FileImageOutputStream(new File("x.gif"));
			
			GifSequenceWriter writer = 
				      new GifSequenceWriter(output, firstImage.getType(), 1, false);

			List<File> args = new ArrayList<File>();
			
			args.add(new File("a.jpeg"));
			args.add(new File("b.jpeg"));
			args.add(new File("c.jpeg"));
			
			
			writer.writeToSequence(firstImage);
		    for(int i=1; i<args.size()-1; i++) {
		      BufferedImage nextImage = ImageIO.read((args.get(i)));
		      writer.writeToSequence(nextImage);
		    }
			
			numcreated++;
			System.out.println("CREATED: " + ficheroAEscribir);

		} catch (IOException e) {
			e.printStackTrace();
		}
		return numcreated;
	}

	private static int createPNGFile(int numcreated, File ficheroAEscribir) {
		try {
			BufferedImage ri = new BufferedImage(2, 2, BufferedImage.TYPE_INT_RGB);

			JPanel canvas = new JPanel() {
				private static final long serialVersionUID = 1L;

				@Override
				protected void paintComponent(Graphics g) {
					super.paintComponent(g);
					g.drawImage(ri, 0, 0, this);
				}
			};

			JFrame frame = new JFrame();
			frame.setLayout(new BorderLayout()); // <== make panel fill frame
			frame.add(canvas, BorderLayout.CENTER);
			frame.setSize(1, 1);
			frame.setVisible(true);
			
			// do you drawing somewhere else, maybe a different thread
			Graphics g = ri.getGraphics();
			g.setColor(Color.white);
			
			g.drawRect(1, 1, 1, 1);
			g.dispose();
			canvas.repaint();

			ImageIO.write(ri, "png", ficheroAEscribir);
			numcreated++;
			System.out.println("CREATED: " + ficheroAEscribir);

		} catch (IOException e) {
			e.printStackTrace();
		}
		return numcreated;
	}

	private static int createJPEGFile(int numcreated, File ficheroAEscribir) {
		try {
			BufferedImage ri = new BufferedImage(2, 2, BufferedImage.TYPE_INT_RGB);

			JPanel canvas = new JPanel() {
				private static final long serialVersionUID = 1L;

				@Override
				protected void paintComponent(Graphics g) {
					super.paintComponent(g);
					g.drawImage(ri, 0, 0, this);
				}
			};

			JFrame frame = new JFrame();
			frame.setLayout(new BorderLayout()); // <== make panel fill frame
			frame.add(canvas, BorderLayout.CENTER);
			frame.setSize(1, 1);
			frame.setVisible(true);
			
			// do you drawing somewhere else, maybe a different thread
			Graphics g = ri.getGraphics();
			g.setColor(Color.white);
			
			g.drawRect(1, 1, 1, 1);
			g.dispose();
			canvas.repaint();

			ImageIO.write(ri, "jpeg", ficheroAEscribir);
			numcreated++;
			System.out.println("CREATED: " + ficheroAEscribir);

		} catch (IOException e) {
			e.printStackTrace();
		}
		return numcreated;
	}

	private static int createPDFFile(int numcreated, File ficheroAEscribir) {
		try {
			PDDocument document = new PDDocument();
			PDPage my_page = new PDPage();
			document.addPage(my_page);
			PDPageContentStream contentStream = new PDPageContentStream(document, my_page);
			contentStream.beginText();
			String text = "Auto-generated PDF file";
			contentStream.showText(text);
			contentStream.endText();
			contentStream.close();
			document.save(ficheroAEscribir);
			document.close();
			numcreated++;
			System.out.println("CREATED: " + ficheroAEscribir);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return numcreated;
	}

	private static int createXMLFile(int numcreated, File ficheroAEscribir) {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(ficheroAEscribir, false));
			bw.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + "<xml>\r\n" + "Auto-generated xml file\r\n"
					+ "</xml>");
			bw.close();
			numcreated++;
			System.out.println("CREATED: " + ficheroAEscribir);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return numcreated;
	}

	private static int createCSSFile(int numcreated, File ficheroAEscribir) {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(ficheroAEscribir, false));
			bw.write("body {\r\n" + "    color: #000000;\r\n" + "}");
			bw.close();
			numcreated++;
			System.out.println("CREATED: " + ficheroAEscribir);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return numcreated;
	}

	private static int createHTMLFile(int numcreated, File ficheroAEscribir) {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(ficheroAEscribir, false));
			bw.write("<!DOCTYPE html>\r\n" + "<html>\r\n" + "<head>\r\n" + "<meta charset=\"UTF-8\"/>\r\n"
					+ "<title>Auto-generated html file</title>\r\n" + "</head>\r\n" + "<body>\r\n"
					+ "<p>Auto-generated html file</p>\r\n" + "</body>\r\n" + "</html>");
			bw.close();
			numcreated++;
			System.out.println("CREATED: " + ficheroAEscribir);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return numcreated;
	}

	private static int createPlainFile(int numcreated, File ficheroAEscribir) {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(ficheroAEscribir, false));
			bw.write("Auto-generated plain file");
			bw.close();
			numcreated++;
			System.out.println("CREATED: " + ficheroAEscribir);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return numcreated;
	}
	*/

	private static List<String> getResults(ResultSet rs) throws SQLException {

		List<String> results = new ArrayList<String>();

		int columnCount = rs.getMetaData().getColumnCount();
		// StringBuilder headers = new StringBuilder();
		// for (int i = 1; i<columnCount; i++)
		// headers.append(rs.getMetaData().getColumnName(i)+ "\t");
		// System.out.println(headers.toString());

		StringBuilder result = null;
		while (rs.next()) {
			result = new StringBuilder();
			for (int i = 1; i < columnCount; i++)
				result.append(rs.getObject(i) + "\t");
			result.append(rs.getObject(columnCount));
			// System.out.println(result.toString());
			results.add(result.toString());
			System.out.println(result.toString());
		}

		if (result == null)
			System.out.println("Not data found");

		return results;

	}

}

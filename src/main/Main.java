package main;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.tuple.MutablePair;
import org.postgresql.Driver;

import javafx.util.Pair;
import oracle.jdbc.driver.OracleDriver;

public class Main {

	static String patternShort = "yyyy-MM-dd";
	static SimpleDateFormat simpleDateFormatShort = new SimpleDateFormat(patternShort);
	static String patternLong = "yyyy-MM-dd'T'HH:mm:ss'Z'";
	static SimpleDateFormat simpleDateFormatLong = new SimpleDateFormat(patternLong);
	
	public static void main(String[] args) {
		if (args == null || args.length != 3) {
			System.out.println("Usage: java -jar stats [dspaceiConfigDir] [fechaInicio{2021-12-31}] [fechaFin(no incluida){2022-12-31}]");
			System.exit(0);
		}
		try {
			String cfgDir = args[0];
			Date startDate = simpleDateFormatShort.parse(args[1]);
			Date endDate = simpleDateFormatShort.parse(args[2]);
			try {
				File fileCfgDir = new File(cfgDir);
				if (fileCfgDir.exists()) {
					Properties propertiesDspaceCfg = new Properties();
					propertiesDspaceCfg.load(new FileReader(cfgDir + "/dspace.cfg"));
					Properties propertiesLocalCfg = new Properties();
					File fLocal=new File(cfgDir + "/local.cfg");
					if(fLocal.exists()){
						propertiesLocalCfg.load(new FileReader(cfgDir + "/local.cfg"));
					}

					String dbUrl = propertiesDspaceCfg.getProperty("db.url");
					String dbUsername = propertiesDspaceCfg.getProperty("db.username");
					String dbPassword = propertiesDspaceCfg.getProperty("db.password");
					String assetstoreDir = propertiesDspaceCfg.getProperty("assetstore.dir");

					if (propertiesLocalCfg.getProperty("db.url") != null) {
						dbUrl = propertiesLocalCfg.getProperty("db.url");
					}
					if (propertiesLocalCfg.getProperty("db.username") != null) {
						dbUsername = propertiesLocalCfg.getProperty("db.username");
					}
					if (propertiesLocalCfg.getProperty("db.password") != null) {
						dbPassword = propertiesLocalCfg.getProperty("db.password");
					}
					if (propertiesLocalCfg.getProperty("assetstore.dir") != null) {
						assetstoreDir = propertiesLocalCfg.getProperty("assetstore.dir");
					}
					// por si el assetstore tiene ${algo}
					assetstoreDir = variableAssetstore(assetstoreDir, propertiesDspaceCfg, propertiesLocalCfg);

					DriverManager.registerDriver(new Driver());
					DriverManager.registerDriver(new OracleDriver());

					Connection con = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
					StringBuilder query = new StringBuilder();
					query.append("select * from metadatavalue mv, metadatafieldregistry mfr where\r\n"
							+ "mv.metadata_field_id=mfr.metadata_field_id and\r\n"
							+ "mfr.element='description' and\r\n"
							+ "mfr.qualifier='provenance' and\r\n"
							+ "mv.text_value like 'Approved%'");
					Statement st = con.createStatement();
					ResultSet rs = st.executeQuery(query.toString());

					Hashtable<String,MutablePair<String,Integer>> results = (getResults(rs,startDate,endDate));
					Enumeration<String> keys = results.keys();
					while(keys.hasMoreElements()) {
						String name = keys.nextElement();
						Integer num = results.get(name).getRight();
						System.out.println(name+":"+num);						
					}
					
					rs.close();
					st.close();
					con.close();
				}

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Muy mejorable. Solo admite una variable, y faltan comprobaciones
	private static String variableAssetstore(String assetstoreDir, Properties propertiesDspaceCfg,
			Properties propertiesLocalCfg) {
		if (assetstoreDir.indexOf("${") != -1 && assetstoreDir.indexOf("}") != -1) {
			String var = assetstoreDir.substring(assetstoreDir.indexOf("${") + 2, assetstoreDir.indexOf("}"));
			String varValue = propertiesLocalCfg.getProperty(var);
			if (varValue == null) {
				varValue = propertiesDspaceCfg.getProperty(var);
			}
			if (varValue != null) {
				return assetstoreDir.replace("${" + var + "}", varValue);

			} else {
				System.out.println("Parametrizado pero falta en la configuracion la variable:" + var);
				return assetstoreDir;
			}
		} else {
			return assetstoreDir;
		}
	}

	

	private static Hashtable<String,MutablePair<String,Integer>> getResults(ResultSet rs,Date startDate,Date endDate) throws SQLException, ParseException {

		Hashtable<String,MutablePair<String,Integer>> results = new Hashtable<String, MutablePair<String, Integer>>();

		StringBuilder result = null;
		while (rs.next()) {
			String provenance = rs.getString("text_value");
			String provenanceDateString = provenance.substring(provenance.indexOf("(GMT)")-21,provenance.indexOf("(GMT)")-1);
			Date provenanceDate = simpleDateFormatLong.parse(provenanceDateString); 
			if(provenanceDate.before(endDate) && provenanceDate.after(startDate)) {
				String name = provenance.substring(provenance.indexOf("(")+1,provenance.indexOf(")"));
				if(results.containsKey(name)) {
					results.get(name).setRight(results.get(name).getRight()+1);
				}else {
					results.put(name, new MutablePair<String, Integer>(name,1));
				}
				
			}
			
		}

		if (results.isEmpty())
			System.out.println("Not data found");

		return results;

	}

}
